Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: PyPPMd
Source: https://codeberg.org/miurahr/pyppmd
Files-Excluded: src/pyppmd.egg-info

Files: *
Copyright: Copyright (C) 2020-2021 Hiroshi Miura
           Copyright (C) 2020-2021 Ma Lin
           Copyright (C) 2010-2012 Lockless Inc.
           Copyright (C) 1999-2017 Igor Pavlov
License: LGPL-2.1+

Files: src/lib/ppmd/*
Copyright: Copyright (C) 1999-2017 Igor Pavlov
License: LGPL-2.1+

Files: src/lib/ppmd/Interface.h
       src/lib/ppmd/Ppmd.h
       src/lib/ppmd/Ppmd7.c
       src/lib/ppmd/Ppmd7.h
       src/lib/ppmd/Ppmd7Dec.c
       src/lib/ppmd/Ppmd7Enc.c
       src/lib/ppmd/Ppmd8.c
       src/lib/ppmd/Ppmd8.h
       src/lib/ppmd/Ppmd8Dec.c
       src/lib/ppmd/Ppmd8Enc.c
Copyright: Igor Pavlov
           Dmitry Shkarin
           Dmitry Subbotin
License: Public_domain
 2017-04-03 : Igor Pavlov : Public domain
 PPMd var.H (2001): Dmitry Shkarin : Public domain
 PPMd var.I (2002): Dmitry Shkarin : Public domain
 Carryless rangecoder (1999): Dmitry Subbotin : Public domain

Files: src/lib/buffer/win_pthreads.h
Copyright: (C) 2010 Lockless Inc. All rights reserved.
License: BSD-3-Clause
 (C) 2010 Lockless Inc.
 All rights reserved.
 .
 Redistribution and use in source and binary forms, with or without modification,
 are permitted provided that the following conditions are met:
 .
 .
  * Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
  * Neither the name of Lockless Inc. nor the names of its contributors may be
    used to endorse or promote products derived from this software without
    specific prior written permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AN
 ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
 BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
 OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 OF THE POSSIBILITY OF SUCH DAMAGE.

Files: debian/*
Copyright: Copyright (C) YOKOTA Hiroshi <yokota.hgml@gmail.com>
License: LGPL-2.1+

License: LGPL-2.1+
 On Debian systems you can find a copy of the GNU LESSER GENERAL PUBLIC
 LICENSE Version 2.1 at /usr/share/common-licenses/LGPL-2.1
